package main

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"

	"gitlab.com/zygoon/indigo"
)

func main() {
	if err := indigo.Run(os.Args[1:], os.Args[0]); err != nil {
		if err, ok := err.(*exec.ExitError); err != nil && ok {
			if ws, ok := err.ProcessState.Sys().(syscall.WaitStatus); ok {
				os.Exit(ws.ExitStatus())
			}
		}
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}
