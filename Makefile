VERSION = 0.2

.PHONY: tarball
tarball: indigo-$(VERSION).tar.gz

indigo-$(VERSION).tar.gz: $(shell find . -name '*.go') Makefile README.md
	git archive --format=tar --prefix indigo-$(VERSION)/ -o $@ HEAD
