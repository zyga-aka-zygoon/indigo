package indigo_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/indigo"
)

type toolchainSuite struct{}

var _ = Suite(&toolchainSuite{})

func (s *toolchainSuite) TestString(c *C) {
	tc := indigo.Toolchain{GoTool: "/usr/bin/go"}
	c.Check(tc.String(), Equals, "golang installation at /usr/bin/go")

	tc = indigo.Toolchain{Vendor: "openSUSE", Version: "1.10"}
	c.Check(tc.String(), Equals, "golang 1.10 by openSUSE")
}

func (s *toolchainSuite) TestGoCmd(c *C) {
	// A plain toolchain will not behave in an unusual way.
	tc := indigo.Toolchain{GoTool: "/usr/bin/go"}
	cmd := tc.GoCmd([]string{"build", "foo.go"})

	c.Check(cmd.Path, Equals, tc.GoTool)
	c.Check(cmd.Args, DeepEquals, []string{tc.GoTool, "build", "foo.go"})
	c.Check(cmd.ExtraFiles, HasLen, 0)
	c.Check(cmd.Dir, Equals, "")

	c.Check(cmd.Stdin, Equals, os.Stdin)
	c.Check(cmd.Stdout, Equals, os.Stdout)
	c.Check(cmd.Stderr, Equals, os.Stderr)
	c.Check(cmd.Env, DeepEquals, os.Environ())

	// If we define any of GOOS, GOARCH, GOROOT, GOPATH or GOBIN those are
	// injected into the environment of the go command.
	for _, name := range []string{"GOOS", "GOARCH", "GOROOT", "GOPATH", "GOBIN"} {
		tc := &indigo.Toolchain{GoTool: "/usr/bin/go"}
		field := reflect.ValueOf(tc).Elem().FieldByName(name)
		field.SetString("altered")

		cmd := tc.GoCmd([]string{"build", "foo.go"})
		// TODO: use Contains once merged upstream.
		expected := fmt.Sprintf("%s=altered", name)
		found := false
		for _, env := range cmd.Env {
			if env == expected {
				found = true
				break
			}
		}
		c.Check(found, Equals, true, Commentf("cannot find expected in %q", cmd.Env))
	}
}

func (s *toolchainSuite) TestLoadToolchain(c *C) {
	d := c.MkDir()

	fname := filepath.Join(d, "test.json")
	_, err := indigo.LoadToolchain(fname)
	c.Check(os.IsNotExist(err), Equals, true)

	c.Assert(ioutil.WriteFile(fname, []byte(`corrupt`), 0644), IsNil)
	_, err = indigo.LoadToolchain(fname)
	c.Assert(err, ErrorMatches, `cannot load toolchain from ".*/test\.json": .*`)

	data, err := json.Marshal(map[string]interface{}{
		"vendor":  "openSUSE",
		"version": "1.10",
		"go":      "/usr/lib64/go/1.10/bin/go",
		"GOARCH":  "amd64",
		"GOOS":    "linux",
		"GOROOT":  "/usr/lib64/go/1.10",
		"GOPATH":  "/usr/share/go/1.10/contrib",
		"GOBIN":   "/usr/bin",
		"policy": map[string]interface{}{
			"build-flags": []interface{}{"-buildmode", "pie"},
		},
	})
	c.Assert(err, IsNil)
	c.Assert(ioutil.WriteFile(fname, data, 0644), IsNil)

	tc, err := indigo.LoadToolchain(fname)
	c.Assert(err, IsNil)
	c.Check(tc.Vendor, Equals, "openSUSE")
	c.Check(tc.Version, Equals, "1.10")
	c.Check(tc.GoTool, Equals, "/usr/lib64/go/1.10/bin/go")
	c.Check(tc.GOARCH, Equals, "amd64")
	c.Check(tc.GOOS, Equals, "linux")
	c.Check(tc.GOROOT, Equals, "/usr/lib64/go/1.10")
	c.Check(tc.GOPATH, Equals, "/usr/share/go/1.10/contrib")
	c.Check(tc.GOBIN, Equals, "/usr/bin")
	c.Check(tc.Policy.BuildFlags, DeepEquals, []string{"-buildmode", "pie"})
}

func (s *toolchainSuite) TestDefinedToolchains(c *C) {
	// The directories may be absent without error.
	var adminToolchains, sysToolchains map[string]*indigo.Toolchain
	restore := MockDefinedToolchains(c, sysToolchains, adminToolchains)
	tcs, err := indigo.DefinedToolchains()
	c.Assert(err, IsNil)
	c.Check(tcs, HasLen, 0)
	restore()

	// Defined toolchains are loaded as expected.
	sysToolchains = map[string]*indigo.Toolchain{
		"go-1.11": {
			Vendor:  "Big Blue Hat",
			Version: "1.11",
			GoTool:  "/usr/lib/golang/bin/go",
		},
	}
	adminToolchains = map[string]*indigo.Toolchain{
		"go-1.12": {
			Vendor:  "Google",
			Version: "1.12",
			GoTool:  "/usr/local/go/bin/go",
		},
	}
	restore = MockDefinedToolchains(c, sysToolchains, adminToolchains)
	tcs, err = indigo.DefinedToolchains()
	c.Assert(err, IsNil)
	c.Check(tcs, DeepEquals, map[string]*indigo.Toolchain{
		"go-1.11": sysToolchains["go-1.11"],
		"go-1.12": adminToolchains["go-1.12"],
	})
	restore()

	// Toolchains can be masked with symlinks to /dev/null
	adminToolchains["go-1.12"] = nil
	restore = MockDefinedToolchains(c, sysToolchains, adminToolchains)
	tcs, err = indigo.DefinedToolchains()
	c.Assert(err, IsNil)
	c.Check(tcs, DeepEquals, map[string]*indigo.Toolchain{
		"go-1.11": sysToolchains["go-1.11"],
	})
}

func (s *toolchainSuite) TestSystemToolchain(c *C) {
	// Using the INDIGO environment variable we can pick one of several toolchains.
	sysToolchains := map[string]*indigo.Toolchain{
		"go-1.10": {GoTool: "/usr/lib/go-1.10/bin/go", Vendor: "Vendor"},
		"go-1.11": {GoTool: "/usr/lib/go-1.11/bin/go", Vendor: "Vendor"},
	}
	restore1 := MockDefinedToolchains(c, sysToolchains, nil)
	restore2 := MockEnv(c, "INDIGO=go-1.10")
	tc, err := indigo.SystemToolchain()
	c.Assert(err, IsNil)
	c.Check(tc, DeepEquals, &indigo.Toolchain{GoTool: "/usr/lib/go-1.10/bin/go", Vendor: "Vendor"})
	restore1()
	restore2()

	// Pointing to absent toolchain returns an error.
	restore1 = MockDefinedToolchains(c, sysToolchains, nil)
	restore2 = MockEnv(c, "INDIGO=go-1.9")
	tc, err = indigo.SystemToolchain()
	c.Assert(err, ErrorMatches, `cannot find toolchain "go-1.9"`)
	c.Check(tc, IsNil)
	restore1()
	restore2()

	// When INDIGO is unset we look up what "go" points to and try to match
	// that to a toolchain. For this purpose we set up a fake "$d/go -> %d/go-foo"
	// symlink and executable it points to.
	d := c.MkDir()
	c.Assert(ioutil.WriteFile(filepath.Join(d, "go-foo"), []byte("#!/bin/sh\n"), 0755), IsNil)
	c.Assert(os.Symlink(filepath.Join(d, "go-foo"), filepath.Join(d, "go")), IsNil)
	sysToolchains = map[string]*indigo.Toolchain{
		"go-foo": {GoTool: filepath.Join(d, "go-foo"), Vendor: "Foo"},
	}
	restore1 = MockDefinedToolchains(c, sysToolchains, nil)
	restore2 = MockEnv(c, "INDIGO=", "PATH="+d)

	tc, err = indigo.SystemToolchain()
	c.Assert(err, IsNil)
	c.Check(tc, DeepEquals, sysToolchains["go-foo"])
	restore1()
	restore2()

	// When INDIGO is unset and there are no definitions but "go" is on PATH we
	// synthesize a minimal definition for it.
	d = c.MkDir()
	c.Assert(ioutil.WriteFile(filepath.Join(d, "go"), []byte("#!/bin/sh\n"), 0755), IsNil)
	restore1 = MockDefinedToolchains(c, nil, nil)
	restore2 = MockEnv(c, "INDIGO=", "PATH="+d)

	tc, err = indigo.SystemToolchain()
	c.Assert(err, IsNil)
	c.Check(tc, DeepEquals, &indigo.Toolchain{GoTool: filepath.Join(d, "go")})
	restore1()
	restore2()

	// Lastly if there are no toolchains defined and when "go" is not on PATH
	// we return an actual error.
	d = c.MkDir()
	restore1 = MockDefinedToolchains(c, nil, nil)
	restore2 = MockEnv(c, "INDIGO=", "PATH="+d)

	tc, err = indigo.SystemToolchain()
	c.Assert(err, ErrorMatches, `cannot find system installation of go: .*`)
	c.Check(tc, IsNil)
	restore1()
	restore2()

}
