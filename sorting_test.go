package indigo_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/indigo"
)

type sortingSuite struct{}

var _ = Suite(&sortingSuite{})

func (s *sortingSuite) TestSortingByPatternLength(c *C) {
	list := []*indigo.CommandOverride{
		{Pattern: []string{"foo"}},
		{Pattern: []string{"foo", "bar"}},
	}
	indigo.SortByPatternLength(list)
	c.Check(list, DeepEquals, []*indigo.CommandOverride{
		{Pattern: []string{"foo", "bar"}},
		{Pattern: []string{"foo"}},
	})
}
