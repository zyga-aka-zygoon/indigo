package indigo_test

import (
	"bytes"
	"strings"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/indigo"
)

type cmdSuite struct {
	tc        *indigo.Toolchain
	stdout    bytes.Buffer
	stderr    bytes.Buffer
	restoreIO func()
}

var _ = Suite(&cmdSuite{})

func (s *cmdSuite) SetUpSuite(c *C) {
	s.restoreIO = indigo.MockIO(&s.stdout, &s.stderr)
}

func (s *cmdSuite) TearDownSuite(c *C) {
	if s.restoreIO != nil {
		s.restoreIO()
	}
}

func (s *cmdSuite) SetUpTest(c *C) {
	s.tc = &indigo.Toolchain{GoTool: "/usr/bin/go"}
	s.stdout.Reset()
	s.stderr.Reset()
}

func (s *cmdSuite) TestIndigo(c *C) {
	err := indigo.IndigoCmd(s.tc, []string{"indigo"})
	c.Assert(err, IsNil)
	needle := "Indigo is a indirection layer for Go."
	c.Check(strings.Contains(s.stdout.String(), needle), Equals, true)
	c.Check(s.stderr.String(), Equals, "")
}

func (s *cmdSuite) TestToolchain(c *C) {
	err := indigo.IndigoToolchainCmd(s.tc, []string{"indigo", "toolchain"})
	c.Assert(err, IsNil)
	c.Check(s.stdout.String(), Equals, "golang installation at /usr/bin/go\n")
	c.Check(s.stderr.String(), Equals, "")
}

func (s *cmdSuite) TestToolchainDashVAndStuff(c *C) {
	s.tc = &indigo.Toolchain{
		GoTool: "/usr/bin/go",
		GOARCH: "arch",
		GOOS:   "os",
		GOROOT: "root",
		GOPATH: "path",
		GOBIN:  "bin",
	}
	err := indigo.IndigoToolchainCmd(s.tc, []string{"indigo", "toolchain", "-v"})
	c.Assert(err, IsNil)
	c.Check(s.stdout.String(), Equals, `golang installation at /usr/bin/go
  GOARCH: arch
  GOOS: os
  GOROOT: root
  GOPATH: path
  GOBIN: bin
`)
	c.Check(s.stderr.String(), Equals, "")
}

func (s *cmdSuite) TestPolicy(c *C) {
	s.tc = &indigo.Toolchain{
		Vendor:  "vendor",
		Version: "1.2.3",
		GoTool:  "/usr/bin/go",
		GOARCH:  "arch",
		GOOS:    "os",
		GOROOT:  "root",
		GOPATH:  "path",
		GOBIN:   "bin",
	}
	s.tc.Policy.BuildFlags = []string{"-buildmode", "pie"}
	err := indigo.IndigoPolicyCmd(s.tc, []string{"indigo", "policy"})
	c.Assert(err, IsNil)
	c.Check(s.stdout.String(), Equals, `toolchain:
  vendor: vendor
  version: 1.2.3
  go-tool: /usr/bin/go
policy:
  build-flags: -buildmode pie
environment:
  GOARCH: arch
  GOOS: os
  GOROOT: root
  GOPATH: path
  GOBIN: bin
`)
	c.Check(s.stderr.String(), Equals, "")
}
