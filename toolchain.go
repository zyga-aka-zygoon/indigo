package indigo

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// Toolchain describes an installation of a golang toolchain.
//
// A typical distribution toolchain is hidden in some directory that is not on
// PATH and exposed to the developer as either an _alternatives_ system or via
// some other method. Distributions may also provide a GOPATH setting that is
// appropriate for the toolchain, where packages are provided as compiled
// binaries compatible with that toolchain.
type Toolchain struct {
	// Vendor identifies the vendor or distributor of the golang installation.
	Vendor string `json:"vendor"`
	// Version is the upstream version of the golang toolchain.
	Version string `json:"version,omitempty"`
	// GoTool is the full path of the go executable.
	GoTool string `json:"go"`

	GOARCH string `json:"GOARCH,omitempty"`
	GOOS   string `json:"GOOS,omitempty"`
	GOROOT string `json:"GOROOT,omitempty"`
	GOPATH string `json:"GOPATH,omitempty"`
	GOBIN  string `json:"GOBIN,omitempty"`

	// Policy contains build policy associated with the toolchain.
	Policy struct {
		// BuildFlags contains additional flags to inject to "go build", if any.
		BuildFlags []string `json:"build-flags,omitempty"`
	} `json:"policy"`
}

// String returns a brief description of the toolchain.
func (toolchain *Toolchain) String() string {
	if toolchain.Version != "" && toolchain.Vendor != "" {
		return fmt.Sprintf("golang %s by %s", toolchain.Version, toolchain.Vendor)
	}
	return fmt.Sprintf("golang installation at %s", toolchain.GoTool)
}

func (toolchain *Toolchain) goCmd(args []string) *exec.Cmd {
	cmd := exec.Command(toolchain.GoTool, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Env = os.Environ()

	// We can just append new values because of how environment handling works.
	if toolchain.GOOS != "" {
		cmd.Env = append(cmd.Env, "GOOS="+toolchain.GOOS)
	}
	if toolchain.GOARCH != "" {
		cmd.Env = append(cmd.Env, "GOARCH="+toolchain.GOARCH)
	}
	if toolchain.GOROOT != "" {
		cmd.Env = append(cmd.Env, "GOROOT="+toolchain.GOROOT)
	}
	if toolchain.GOPATH != "" {
		cmd.Env = append(cmd.Env, "GOPATH="+toolchain.GOPATH)
	}
	if toolchain.GOBIN != "" {
		cmd.Env = append(cmd.Env, "GOBIN="+toolchain.GOBIN)
	}
	return cmd
}

// Go runs the go executable.
func (toolchain *Toolchain) Go(args []string) error {
	cmd := toolchain.goCmd(args)
	return cmd.Run()
}

// LoadToolchain reads toolchain declaration and returns a toolchain object.
func LoadToolchain(fname string) (*Toolchain, error) {
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	dec := json.NewDecoder(f)
	var toolchain Toolchain
	if err := dec.Decode(&toolchain); err != nil {
		return nil, fmt.Errorf("cannot load toolchain from %q: %s", fname, err)
	}
	return &toolchain, nil
}

func isToolchain(fname string) (ok bool, err error) {
	var fi os.FileInfo
	fi, err = os.Lstat(fname)
	if err != nil {
		return false, err
	}
	if fi.Mode()&os.ModeSymlink != 0 {
		var target string
		target, err = os.Readlink(fname)
		if err != nil {
			return false, err
		}
		if target == "/dev/null" {
			return false, nil
		}
	}
	return fi.Mode().IsRegular(), nil
}

var (
	systemToolchainGlob = "/usr/lib/indigo/*.json"
	adminToolchainGlob  = "/etc/indigo/*.json"
)

// DefinedToolchains enumerates and loads toolchains present in the system.
//
// Toolchains are described by .json files in /usr/lib/indigo and /etc/indigo.
// The files in /etc take precedence over those in /usr/lib. A toolchain
// declaration in /etc/indigo/foo.json overrides that in
// /usr/lib/indigo/foo.json. A toolchain declaration can be disabled with a
// symlink to /dev/null.
func DefinedToolchains() (map[string]*Toolchain, error) {
	m1, err := filepath.Glob(systemToolchainGlob)
	if err != nil {
		return nil, err
	}
	m2, err := filepath.Glob(adminToolchainGlob)
	if err != nil {
		return nil, err
	}
	fnames := append(m1, m2...)
	toolchains := make(map[string]*Toolchain, len(fnames))
	for _, fname := range fnames {
		name := filepath.Base(fname)
		ext := filepath.Ext(name)
		name = strings.TrimSuffix(name, ext)
		ok, err := isToolchain(fname)
		if err != nil {
			return nil, err
		}
		if ok {
			toolchain, err := LoadToolchain(fname)
			if err != nil {
				return nil, err
			}
			toolchains[name] = toolchain
		} else {
			delete(toolchains, name)
		}
	}
	return toolchains, nil
}

// SystemToolchain returns the system toolchain.
//
// The system toolchain can be overridden by the INDIGO environment variable.
// If used it must contain the name of a toolchain declaration installed on
// the system.
//
// If the default "go" binary corresponds to a known toolchain that toolchain
// as well as the associated policy is automatically found.
func SystemToolchain() (*Toolchain, error) {
	toolchains, err := DefinedToolchains()
	if err != nil {
		return nil, err
	}

	// If the INDIGO variable is defined find the toolchain matching it.
	if name := os.Getenv("INDIGO"); name != "" {
		if toolchain, ok := toolchains[name]; ok {
			return toolchain, nil
		}
		return nil, fmt.Errorf("cannot find toolchain %q", name)
	}

	// If "go" is on PATH then look for it in the known toolchains.
	// Distributions sometimes use /etc/alternatives to make /usr/bin/go
	// a symlink to /etc/alternatives/go which points to the real toolchain.
	// Doing this allows us to know the policy associated with that toolchain.
	path, err := exec.LookPath("go")
	if err != nil {
		return nil, fmt.Errorf("cannot find system installation of go: %s", err)
	}
	realPath, err := filepath.EvalSymlinks(path)
	if err != nil {
		return nil, fmt.Errorf("cannot evaluate symlinks in %q: %s", path, err)
	}
	for _, toolchain := range toolchains {
		if realPath == toolchain.GoTool {
			return toolchain, nil
		}
	}

	// If all else fails, just return the vanilla toolchain we found.
	return &Toolchain{GoTool: path}, nil
}
