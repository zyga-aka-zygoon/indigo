package indigo

// CommandOverride describes an override to a "go" command.
type CommandOverride struct {
	Pattern []string
	Action  func(toolchain *Toolchain, args []string) error
}

// Matches returns true if the override matches a given command.
func (co *CommandOverride) Matches(args []string) bool {
	if len(co.Pattern) > len(args) {
		return false
	}
	for i := 0; i < len(co.Pattern); i++ {
		if co.Pattern[i] != args[i] {
			return false
		}
	}
	return true
}
