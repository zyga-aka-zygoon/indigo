package indigo

// byPatternLength allows sorting CommandOverride by length of the command pattern.
type byPatternLength []*CommandOverride

func (c byPatternLength) Len() int           { return len(c) }
func (c byPatternLength) Less(i, j int) bool { return len(c[i].Pattern) > len(c[j].Pattern) }
func (c byPatternLength) Swap(i, j int)      { c[i], c[j] = c[j], c[i] }
