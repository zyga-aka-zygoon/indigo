package indigo_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/indigo"
)

type overrideSuite struct{}

var _ = Suite(&overrideSuite{})

func (s *overrideSuite) TestMatches(c *C) {
	co := &indigo.CommandOverride{Pattern: []string{"go", "build"}}
	c.Check(co.Matches([]string{"go", "build", "stuff.go"}), Equals, true)
	c.Check(co.Matches([]string{"go", "build"}), Equals, true)
	c.Check(co.Matches([]string{"go"}), Equals, false)
	c.Check(co.Matches([]string{"go", "install"}), Equals, false)
	c.Check(co.Matches([]string{"pogo", "build", "stuff.go"}), Equals, false)
}
