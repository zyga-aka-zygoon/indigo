// indigo is an indirection layer for golang
//
// Indigo aims to fill a niche in downstream packaging of golang-based software
// in various software distributions. Distributions feel the need to create
// complex layers of software around the underlying toolchain (either go or
// gccgo) in order to enforce packaging policy such as hardening builds or use
// of packaged libraries.
//
// Such packaging wrappers are of varying quality and feel foreign to
// developers of golang projects due to their total absence of use outside of
// distribution build system. It is often difficult to even understand what a
// given wrapper actually does as it is implemented as a part of the package
// management build system which few people are deeply familiar with.
//
// Indigo is a drop-in replacement for the "go" command used by virtually all
// the golang developers. It acts as a pass-through to whichever toolchain is
// selected by the developer. Unlike plain go, indigo understands the
// requirements of downstream packaging such as compiler hardening flags.
//
// Golang developers can trivially install indigo and use it on their system to
// see how a build would behave at distribution package build time. They can
// also easily examine the distribution policy from a set of declarative
// statements provided by the distribution they choose to run indigo on.
//
// For more information about indigo please run "indigo help" or "go help
// indigo".
package indigo
