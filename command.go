package indigo

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

const indigoHelp = `
Indigo is a indirection layer for Go.

Usage:

	go command [arguments]

Indigo may be installed as a substitute of the "go" command where it invokes
appropriate "go" tool directly. Some commands are overridden so that
distribution build policy is applied.

	indigo command [arguments]

Indigo may be installed as a stand-alone command where it supports just the
commands that are customized.

Commands changed by indigo are:

	build		build flags described by the policy are applied

Commands specific to indigo are:

	toolchain 	print information about the used toolchain
	policy		print details of the distribution policy

Indigo respects the INDIGO environment variable for toolchain selection.
`

var (
	stdout io.Writer = os.Stdout
	stderr io.Writer = os.Stderr
)

func indigoCmd(toolchain *Toolchain, args []string) error {
	fmt.Fprint(stdout, indigoHelp)
	return nil
}

func show(name, value string) {
	if value != "" {
		fmt.Fprintf(stdout, "  %s: %s\n", name, value)
	}
}

func indigoToolchainCmd(toolchain *Toolchain, args []string) error {
	verbose := len(args) >= 1 && args[len(args)-1] == "-v"
	fmt.Fprintf(stdout, "%s\n", toolchain)
	if !verbose {
		return nil
	}
	show("GOARCH", toolchain.GOARCH)
	show("GOOS", toolchain.GOOS)
	show("GOROOT", toolchain.GOROOT)
	show("GOPATH", toolchain.GOPATH)
	show("GOBIN", toolchain.GOBIN)
	return nil
}

func indigoPolicyCmd(toolchain *Toolchain, args []string) error {
	fmt.Fprintf(stdout, "toolchain:\n")
	fmt.Fprintf(stdout, "  vendor: %s\n", toolchain.Vendor)
	fmt.Fprintf(stdout, "  version: %s\n", toolchain.Version)
	fmt.Fprintf(stdout, "  go-tool: %s\n", toolchain.GoTool)
	if len(toolchain.Policy.BuildFlags) > 0 {
		fmt.Fprintf(stdout, "policy:\n")
		show("build-flags", strings.Join(toolchain.Policy.BuildFlags, " "))
	}
	if toolchain.GOARCH != "" || toolchain.GOOS != "" || toolchain.GOROOT != "" || toolchain.GOPATH != "" || toolchain.GOBIN != "" {
		fmt.Fprintf(stdout, "environment:\n")
		show("GOARCH", toolchain.GOARCH)
		show("GOOS", toolchain.GOOS)
		show("GOROOT", toolchain.GOROOT)
		show("GOPATH", toolchain.GOPATH)
		show("GOBIN", toolchain.GOBIN)
	}
	return nil
}

func indigoBuildOrInstallCmd(toolchain *Toolchain, args []string) error {
	if len(toolchain.Policy.BuildFlags) > 0 {
		newArgs := args[:1]
		newArgs = append(newArgs, toolchain.Policy.BuildFlags...)
		newArgs = append(newArgs, args[1:]...)
		args = newArgs
	}
	return toolchain.Go(args)
}

// overrides contains all of the changes that indigo applies to go.
var overrides []*CommandOverride = []*CommandOverride{{
	Pattern: []string{"indigo"},
	Action:  indigoCmd,
}, {
	Pattern: []string{"indigo", "toolchain"},
	Action:  indigoToolchainCmd,
}, {
	Pattern: []string{"indigo", "policy"},
	Action:  indigoPolicyCmd,
}, {
	Pattern: []string{"build"},
	Action:  indigoBuildOrInstallCmd,
}, {
	Pattern: []string{"install"},
	Action:  indigoBuildOrInstallCmd,
}}

func init() {
	// Sort overrides by pattern length. This way the first override that
	// matches is the most specific one.
	sort.Sort(byPatternLength(overrides))
}

// Run runs the indigo command, as if invoked from command line.
func Run(args []string, argv0 string) error {
	toolchain, err := SystemToolchain()
	if err != nil {
		return err
	}

	if filepath.Base(argv0) == "indigo" {
		args = append([]string{"indigo"}, args...)
	}

	for _, override := range overrides {
		if override.Matches(args) {
			return override.Action(toolchain, args)
		}
	}

	if filepath.Base(argv0) == "go" {
		return toolchain.Go(args)
	}

	return nil
}
