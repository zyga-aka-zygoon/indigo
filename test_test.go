package indigo_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/indigo"
)

func MockDefinedToolchains(c *C, system, admin map[string]*indigo.Toolchain) (restore func()) {
	d := c.MkDir()

	dirs := []struct {
		prefix     string
		toolchains map[string]*indigo.Toolchain
	}{
		{"sys", system},
		{"admin", admin},
	}

	for _, dir := range dirs {
		if len(dir.toolchains) == 0 {
			continue
		}
		c.Assert(os.MkdirAll(filepath.Join(d, dir.prefix), 0755), IsNil)
		for name, tc := range dir.toolchains {
			fname := filepath.Join(d, dir.prefix, name) + ".json"
			if tc == nil {
				c.Assert(os.Symlink("/dev/null", fname), IsNil)
			} else {
				data, err := json.Marshal(tc)
				c.Assert(err, IsNil)
				c.Assert(ioutil.WriteFile(fname, data, 0644), IsNil)
			}
		}
	}

	restore = indigo.MockToolchainGlobs(
		filepath.Join(d, dirs[0].prefix, "*.json"),
		filepath.Join(d, dirs[1].prefix, "*.json"))

	return restore
}

func MockEnv(c *C, vars ...string) (restore func()) {
	oldVars := make([]string, 0, len(vars))
	for _, kv := range vars {
		split := strings.SplitN(kv, "=", 2)
		c.Assert(split, HasLen, 2)
		k, v := split[0], split[1]
		// Remember old value if one was present.
		if v, ok := os.LookupEnv(k); ok {
			oldVars = append(oldVars, k+"="+v)
		}
		// Set or unset the new value.
		if v != "" {
			c.Assert(os.Setenv(k, v), IsNil)
		} else {
			c.Assert(os.Unsetenv(k), IsNil)
		}
	}
	return func() {
		for _, kv := range vars {
			split := strings.SplitN(kv, "=", 2)
			c.Assert(split, HasLen, 2)
			k := split[0]
			c.Assert(os.Unsetenv(k), IsNil)
		}
		for _, kv := range oldVars {
			split := strings.SplitN(kv, "=", 2)
			c.Assert(split, HasLen, 2)
			k, v := split[0], split[1]
			c.Assert(os.Setenv(k, v), IsNil)
		}
	}
}
