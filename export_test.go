package indigo

import (
	"io"
	"os/exec"
	"sort"
)

func (toolchain *Toolchain) GoCmd(args []string) *exec.Cmd {
	return toolchain.goCmd(args)
}

func MockToolchainGlobs(system, admin string) (restore func()) {
	oldSystem := systemToolchainGlob
	oldAdmin := adminToolchainGlob
	systemToolchainGlob = system
	adminToolchainGlob = admin
	return func() {
		systemToolchainGlob = oldSystem
		adminToolchainGlob = oldAdmin
	}
}

func SortByPatternLength(list []*CommandOverride) {
	sort.Sort(byPatternLength(list))
}

func MockIO(newStdout, newStderr io.Writer) (restore func()) {
	oldStdout := stdout
	oldStderr := stderr
	stdout = newStdout
	stderr = newStderr
	return func() {
		stdout = oldStdout
		stderr = oldStderr
	}
}

var (
	IndigoCmd          = indigoCmd
	IndigoToolchainCmd = indigoToolchainCmd
	IndigoPolicyCmd    = indigoPolicyCmd
)
